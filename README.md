# React TypeScript Webpack Starter

### Stack:

- TypeScript 3.4+
- React 16.8+
- React Router 5.0+
- Jest 24.8+
- Ts-Jest 24.0+
- Enzyme 3.9+
- Express 4.17+
- SCSS
- Webpack 4.31+
- ESLint 5.16+
- Prettier 1.17+

### Scripts:

- build: Builds both the client and server bundles using webpack.
- start: Starts the express server.
- test: Runs the test suites with jest (ts-jest) and enzyme.

### URL:

http://localhost:8080
