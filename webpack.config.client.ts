import * as webpack from "webpack";
import * as path from "path";
import { webpackCommon } from "./webpack.config.common";
import { strategy } from "webpack-merge";
import MiniCssExtractPlugin from "mini-css-extract-plugin";
import UglifyJsPlugin from "uglifyjs-webpack-plugin";
import OptimizeCSSAssetsPlugin from "optimize-css-assets-webpack-plugin";

const extractCSS = new MiniCssExtractPlugin({
  filename: "styles.css"
});

const client: webpack.Configuration = strategy({
  plugins: "prepend",
  entry: "prepend"
})(webpackCommon, {
  mode: "development",
  name: "client",
  target: "web",
  entry: path.resolve(__dirname, "src/client/index.tsx"),
  output: {
    filename: "bundle.client.js",
    path: path.resolve(__dirname, "build"),
    globalObject: "this"
  },
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        cache: true,
        parallel: true,
        sourceMap: false
      }),
      new OptimizeCSSAssetsPlugin({
        cssProcessorOptions: {
          safe: true,
          discardComments: {
            removeAll: true
          }
        }
      })
    ]
  },
  devtool: "inline-source-map",
  module: {
    rules: [
      {
        test: /.*\.tsx$/,
        include: path.resolve("src/client"),
        use: ["react-hot-loader/webpack", "ts-loader"]
      },
      {
        test: /\.scss$/,
        use: [
          "style-loader",
          MiniCssExtractPlugin.loader,
          "css-loader", // translates CSS into CommonJS
          "sass-loader" // compiles Sass to CSS, using Node Sass by default
        ]
      }
    ]
  },
  plugins: [extractCSS],
  devServer: {
    hot: true,
    quiet: true,
    historyApiFallback: true
  }
});

export default client;
