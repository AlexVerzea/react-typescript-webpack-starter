// const webpack = require("webpack");
// const path = require("path");
// const nodeExternals = require("webpack-node-externals");
// const UglifyJsPlugin = require("uglifyjs-webpack-plugin");

// const config = {
//   mode: "development",
//   plugins: [new webpack.HotModuleReplacementPlugin()],
//   module: {
//     rules: [
//       {
//         test: /\.tsx?$/,
//         use: "ts-loader",
//         exclude: /node_modules/
//       },
//       {
//         test: /\.scss$/,
//         use: ["css-loader", "sass-loader"]
//       }
//     ]
//   },
//   resolve: {
//     extensions: [".tsx", ".ts", ".js"],
//     modules: ["src", "node_modules"]
//   },
//   optimization: {
//     minimizer: [
//       new UglifyJsPlugin({
//         uglifyOptions: {
//           keep_fnames: true
//         }
//       })
//     ]
//   }
// };

// const client = Object.assign({}, config, {
//   name: "client",
//   target: "web",
//   entry: path.resolve(__dirname, "src/client/index.tsx"),
//   output: {
//     filename: "clientBundle.js",
//     path: path.resolve(__dirname, "build"),
//     globalObject: "this"
//   }
// });

// const server = Object.assign({}, config, {
//   name: "server",
//   target: "node",
//   externals: [nodeExternals()],
//   entry: path.resolve(__dirname, "src/server/index.tsx"),
//   output: {
//     filename: "serverBundle.js",
//     path: path.resolve(__dirname, "build"),
//     globalObject: "this"
//   }
// });

// module.exports = [client, server];
