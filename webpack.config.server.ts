import * as webpack from "webpack";
import * as path from "path";
import { webpackCommon } from "./webpack.config.common";
import { strategy } from "webpack-merge";
import nodeExternals from "webpack-node-externals";

const server: webpack.Configuration = strategy({
  plugins: "prepend",
  entry: "prepend"
})(webpackCommon, {
  mode: "development",
  name: "server",
  target: "node",
  externals: [nodeExternals()],
  entry: path.resolve(__dirname, "src/server/index.tsx"),
  output: {
    filename: "bundle.server.js",
    path: path.resolve(__dirname, "build"),
    globalObject: "this"
  },
  devtool: false,
  module: {
    rules: [
      {
        test: /.*\.tsx?$/,
        include: path.resolve("src"),
        use: ["ts-loader"]
      },
      {
        test: /\.scss$/,
        use: ["css-loader", "sass-loader"]
      }
    ]
  }
});

export default server;
