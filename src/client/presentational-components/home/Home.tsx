import React, { Component } from "react";
import styles from "./Home.scss";

class Home extends Component {
  render() {
    return <h1 className={styles.home}>Home Page</h1>;
  }
}

export default Home;
