import React from "react";
import { shallow } from "enzyme";
import Home from "./Home";

/**
 * Factory function to create a ShallowWrapper for the Home component.
 * @function setup
 * @returns {ShallowWrapper}
 */
const setup = () => {
  return shallow(<Home />);
};

describe("Home: Initial Rendering", () => {
  it("renders the Home component", () => {
    const wrapper = setup();
    expect(wrapper.length).toBe(1);
  });
});
