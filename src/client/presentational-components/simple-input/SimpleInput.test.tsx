import React from "react";
import { shallow } from "enzyme";
import SimpleInput from "./SimpleInput";

/**
 * Factory function to create a ShallowWrapper for the SimpleInput component.
 * @function setup
 * @returns {ShallowWrapper}
 */
const setup = () => {
  return shallow(<SimpleInput />);
};

describe("SimpleInput: Initial Rendering", () => {
  it("renders the SimpleInput component", () => {
    const wrapper = setup();
    expect(wrapper.length).toBe(1);
  });
});
