import React, { Component } from "react";

export interface Props {
  propExample: string;
}

export interface State {
  counter: number;
  error: boolean;
}

class Counter extends Component<Props, State> {
  constructor(props: Props) {
    super(props);

    this.state = {
      counter: 0,
      error: false
    };

    /**
     * Need to bind this for decrementCounter and incrementCounter, since they use this.state and this.setState.
     */
    this.decrementCounter = this.decrementCounter.bind(this);
    this.incrementCounter = this.incrementCounter.bind(this);
  }

  decrementCounter(): void {
    if (this.state.counter === 0) {
      this.setState({ error: true });
    } else {
      this.setState({ counter: this.state.counter - 1 });
    }
  }

  incrementCounter(): void {
    if (this.state.error) {
      this.setState({ error: false });
    }
    this.setState({ counter: this.state.counter + 1 });
  }

  render() {
    const errorClass = this.state.error ? "" : "hidden";

    return (
      <div data-test="counter-container">
        <h1 data-test="counter-display">
          The counter is currently {this.state.counter}
        </h1>
        <div data-test="error-message" className={`error ${errorClass}`}>
          The counter cannot go below 0
        </div>
        <button data-test="increment-button" onClick={this.incrementCounter}>
          Increment counter
        </button>
        <button data-test="decrement-button" onClick={this.decrementCounter}>
          Decrement counter
        </button>
      </div>
    );
  }
}

export default Counter;
