import React from "react";
import { shallow, ShallowWrapper } from "enzyme";
import Counter, { Props, State } from "./Counter";

const defaultProps: Props = {
  propExample: "blue"
};

const defaultState: State = {
  counter: 0,
  error: false
};

/**
 * Factory function to create a ShallowWrapper for the Counter component.
 * @function setup
 * @param {object} props - Component props specific to this setup.
 * @param {object} state - Initial state for setup.
 * @returns {ShallowWrapper}
 */
const setup = (
  props: Props = defaultProps,
  state: State = defaultState
): ShallowWrapper => {
  const wrapper = shallow(<Counter {...props} />);
  if (state) wrapper.setState(state);
  return wrapper;
};

/**
 * Return ShallowWrapper containing node(s) with the given data-test value.
 * @param {ShallowWrapper} wrapper - Enzyme shallow wrapper to search within.
 * @param {string} val - Value of data-test attribute for search.
 * @returns {ShallowWrapper}
 */
const findByTestAttr = (
  wrapper: ShallowWrapper,
  val: string
): ShallowWrapper => {
  return wrapper.find(`[data-test="${val}"]`);
};

// Tests related to initial rendering.
describe("Initial Rendering", () => {
  it("renders without error", () => {
    const wrapper = setup();
    const appComponent = findByTestAttr(wrapper, "counter-container");
    expect(appComponent.length).toBe(1);
  });

  it("renders the counter display", () => {
    const wrapper = setup();
    const counterDisplay = findByTestAttr(wrapper, "counter-display");
    expect(counterDisplay.length).toBe(1);
  });

  it("starts the counter at 0", () => {
    const wrapper = setup();
    const initialCounterState = wrapper.state("counter");
    expect(initialCounterState).toBe(0);
  });
});

// Tests related to incrementing the counter.
describe("Increment Counter", () => {
  it("renders the increment button", () => {
    const wrapper = setup();
    const button = findByTestAttr(wrapper, "increment-button");
    expect(button.length).toBe(1);
  });

  it("increments the counter display when clicking the increment button", () => {
    const counter = 7;
    const wrapper = setup(defaultProps, { counter: counter, error: false });

    // Find the increment button and click it.
    const button = findByTestAttr(wrapper, "increment-button");
    button.simulate("click");
    wrapper.update();

    // Find the display and test its value.
    const counterDisplay = findByTestAttr(wrapper, "counter-display");
    expect(counterDisplay.text()).toContain((counter + 1).toString());
  });
});

// Tests related to decrementing the counter.
describe("Decrement Counter", () => {
  it("renders the decrement button", () => {
    const wrapper = setup();
    const button = findByTestAttr(wrapper, "decrement-button");
    expect(button.length).toBe(1);
  });

  describe("Decrement Counter when Counter is > 0", () => {
    it("decrements the counter display when clicking the decrement button", () => {
      const counter = 7;
      const wrapper = setup(defaultProps, {
        counter: counter,
        error: false
      });

      // Find the decrement button and click it.
      const button = findByTestAttr(wrapper, "decrement-button");
      button.simulate("click");
      wrapper.update();

      // Find the display and test its value.
      const counterDisplay = findByTestAttr(wrapper, "counter-display");
      expect(counterDisplay.text()).toContain((counter - 1).toString());
    });

    it("doesn't show the error when not needed", () => {
      // No need to set counter value here; default value of 0 is good.
      const wrapper = setup();
      const errorDiv = findByTestAttr(wrapper, "error-message");
      const errorHasHiddenClass = errorDiv.hasClass("hidden");
      expect(errorHasHiddenClass).toBe(true);
    });
  });

  describe("Decrement Counter when Counter is = 0", () => {
    // Scoping wrapper to the describe, so it can be used in beforeEach and the tests.
    let wrapper: ShallowWrapper;

    beforeEach(() => {
      // No need to set counter value here; default value of 0 is good.
      wrapper = setup();

      // Find the decrement button and click it.
      const button = findByTestAttr(wrapper, "decrement-button");
      button.simulate("click");
      wrapper.update();
    });
    it("shows the error", () => {
      const errorDiv = findByTestAttr(wrapper, "error-message");
      const errorHasHiddenClass = errorDiv.hasClass("hidden");
      expect(errorHasHiddenClass).toBe(false);
    });
    it("still displays 0 for the counter", () => {
      const counterDisplay = findByTestAttr(wrapper, "counter-display");
      expect(counterDisplay.text()).toContain(0);
    });
    test("clears the error when clicking increment", () => {
      // Find the increment button and click it.
      const button = findByTestAttr(wrapper, "increment-button");
      button.simulate("click");

      // Check the class of the error message.
      const errorDiv = findByTestAttr(wrapper, "error-message");
      const errorHasHiddenClass = errorDiv.hasClass("hidden");
      expect(errorHasHiddenClass).toBe(true);
    });
  });
});
