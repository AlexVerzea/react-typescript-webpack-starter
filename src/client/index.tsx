import { AppContainer } from "react-hot-loader";
import * as React from "react";
import * as ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import { Routes } from "./routes";

/**
 * Hydrate the static HTML returned from the server and turn it into a dynamic DOM. React
 * will add event listeners allowing the app to react to client-side data changes.
 */
function reactRender(Routes: any) {
  const renderMethod = !!module.hot ? ReactDOM.render : ReactDOM.hydrate;
  renderMethod(
    <AppContainer>
      <BrowserRouter basename="/">
        <Routes />
      </BrowserRouter>
    </AppContainer>,
    document.getElementById("app")
  );
}

reactRender(Routes);

if (module.hot) {
  console.log("module hots!");
  module.hot.accept("./routes", () => {
    const nextApp = require("./routes").Routes;
    console.log(nextApp);
    reactRender(nextApp);
  });
}
