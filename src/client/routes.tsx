import * as React from "react";
import { Switch } from "react-router-dom";
import { Route } from "react-router4-with-layouts";
import Home from "../client/presentational-components/home/Home";
import SimpleInput from "./presentational-components/simple-input/SimpleInput";

export const Routes = () => (
  <Switch>
    <Route path="/" component={Home} exact={true} />
    <Route path="/counter" component={SimpleInput} exact={true} />
  </Switch>
);
