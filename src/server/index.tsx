import * as React from "react";
import * as ReactDOM from "react-dom/server";
import { StaticRouter } from "react-router-dom";
import { Routes } from "client/routes";
import express from "express";

function main() {
  const app = express();
  const port = 8080;

  // Serve files from within build directory.
  app.use(express.static("build"));

  app.get("/*", (req, res, next) => {
    // Render the entire app server-side as static HTML.
    const staticHTMLApp = ReactDOM.renderToString(
      <StaticRouter basename="/">
        <Routes />
      </StaticRouter>
    );

    /**
     * Return the string below to the client. It includes the HTML string generated above
     * for the entire app and a reference to the client bundle, containing client-side
     * libraries to be used.
     */
    res.send(`
      <!doctype html>
      <html lang="en-us">
        <head>
          <title>React TypeScript Webpack Starter</title>
          <style>
            body {
              margin: 0px;
              padding: 0px;
            }
          </style>
          <link rel="stylesheet" href="styles.css">
        </head>
        <body>
          <div id="app">${staticHTMLApp}</div>
          <script type="application/javascript" src="bundle.client.js"></script>
        </body>
      </html>
    `);
    res.end();
    next();
  });

  /**
   * Print the host and port where app listens.
   */
  app.listen(port, "localhost", () => {
    console.log(`Dev Server listening on http://localhost:${port}`);
  });
}

main();
