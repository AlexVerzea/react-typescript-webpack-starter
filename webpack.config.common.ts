export const webpackCommon = {
  resolve: {
    extensions: [".tsx", ".ts", ".js"],
    modules: ["src", "node_modules"],
    alias: {
      "react-dom": "@hot-loader/react-dom"
    }
  }
};
